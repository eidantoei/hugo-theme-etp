document.addEventListener("DOMContentLoaded", function(){
  var button = document.querySelector('.post-toc-switch-button');
  button.addEventListener('click', function(e) {
    el = document.querySelector('.post-toc');
    el.classList.toggle('post-toc-hide-mobile');
  });

  var body = document.querySelector('.post-body');
  body.addEventListener('click', function(e) {
    el = document.querySelector('.post-toc');
    el.classList.add('post-toc-hide-mobile');
  });
}, false);
